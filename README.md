# odds-checker

## Description
Monitors odds from multiple bookmakers to provide real-time analytics.

When a pro gambler makes a large bet, the bookmaker will often react by changing the odds.
It is possible at this time that an opportunity exists to place a bet with another bookmaker
at far better odds than the bookmaker that made the reactionary change.

This project aims to provide analysis to help spot these moments of potentially good odds on
markets that are in the process of moving due to a large bet being made (or other event that 
causes a discrepancy across bookmakers) 

## APIs
This project hits the [The Odds Api](https://the-odds-api.com).  It requires an environment 
variable to be set ODDS_API_KEY with the appropriate api key, which can be retrieved from the link 
above.
