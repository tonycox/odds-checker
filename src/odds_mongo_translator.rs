use mongo;
use odds_api;
use models;
use error::BoxResult;
use serde_json;

use bson;
use mongodb::db::ThreadedDatabase;

fn convert_to_bson(event: &models::Event) -> BoxResult<bson::Document> {
    let serialised = serde_json::to_string(&event)?;
    let res: bson::Bson = bson::to_bson(&serialised)?;
    Ok(res.as_document().unwrap().to_owned())
}

pub fn write_event_to_mongo<T: ThreadedDatabase>(db: &T, event: models::Event) -> BoxResult<bool> {
    let doc = convert_to_bson(&event)?;
    let _ = mongo::add_one_to_collection(db, "event", doc)?;
    Ok(true)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn do_nothing() {

    }
}