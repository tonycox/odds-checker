use error::{BoxResult, OddsCheckerError};
use models::{Sport, Region, Site, Event, SiteMapOrVector};
use reqwest::Url;

use serde_json::Value;

use std::collections::HashMap;
use std::env;

const BASE_URL: &str = "https://api.the-odds-api.com/v2/";
const API_KEY_ENV_VAR_KEY: &str = "API_KEY";

fn api_key() -> BoxResult<String> {
    let api_key = &env::var(API_KEY_ENV_VAR_KEY)?;
    Ok(api_key.to_owned())
}

fn make_request(url: &Url) -> BoxResult<Value> {
    let mut response = reqwest::Client::new()
        .get(url.as_str())
        .header("Accept", "application/json")
        .send()?;
    let status = response.status();
    if status.as_u16() != 200 {
        return OddsCheckerError::new(&format!(
            "Request to {} returned a response code {}",
            &url,
            &status.as_str(),
        ))
        .into_box_result();
    }
    Ok(response.json::<serde_json::Value>()?["data"].take())
}

pub fn get_url_with_queries(sub: &str, queries: &[(&str, &str)]) -> BoxResult<Url> {
    let mut url = get_url(sub)?;
    for (name, val) in queries {
        url.query_pairs_mut().append_pair(name, val);
    }
    Ok(url)
}

pub fn get_url(sub: &str) -> BoxResult<Url> {
    let mut url = Url::parse(BASE_URL)?.join(sub)?;
    url.set_query(Some(&format!("apiKey={}", api_key()?)));
    Ok(url)
}

pub fn get_sports() -> BoxResult<Vec<Sport>> {
    let url = get_url("sports")?;
    if let Value::Array(sport_values) = make_request(&url)? {
        Ok(sport_values
            .into_iter()
            .map(|v| serde_json::from_value(v).unwrap())
            .collect())
    } else {
        OddsCheckerError::new("JSON was not in expected format").into_box_result()
    }
}

pub fn get_odds(sport: &str, region: Region) -> BoxResult<Vec<Event>> {
    let queries = vec![("sport", sport), ("region", region.as_str())];
    let url = get_url_with_queries("odds", &queries)?;
    if let Value::Object(mut map) = make_request(&url)?["events"].take() {
        Ok(map
            .values_mut()
            .map(|v| serde_json::from_value(v.take()).unwrap())
            .collect())
    } else {
        OddsCheckerError::new("JSON was not in expected format").into_box_result()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde::{Deserialize, Serialize};


    #[derive(Serialize, Deserialize, Debug, PartialEq)]
    #[serde(untagged)]
    enum TextOrNumber {
        Text(String),
        Number(u32),
    }

    #[derive(Serialize, Deserialize, Debug)]
    pub struct VariableJSON {
        variant: TextOrNumber,
    }

    #[test]
    fn variable_json() {
        use serde_json::json;
        let variant = "John Doe";
        let deserialized: VariableJSON =
            serde_json::from_value(json!({ "variant": &variant })).unwrap();
        assert!(deserialized.variant == TextOrNumber::Text(variant.to_string()));
        let variant = 43;
        let deserialized: VariableJSON =
            serde_json::from_value(json!({ "variant": &variant })).unwrap();
        assert!(deserialized.variant == TextOrNumber::Number(variant));
    }

    #[test]
    fn url() {
        let res = get_url("sports");
        assert!(res.is_ok());
    }

    #[test]
    fn url_with_queries() {
        use std::borrow::Cow;
        let queries = vec![("all", "1")];
        let res = get_url_with_queries("sports", &queries);
        assert!(res.is_ok());
        let url = res.unwrap();
        let mut pairs = url.query_pairs();
        assert_eq!(
            pairs.next(),
            Some((
                Cow::Borrowed("apiKey"),
                Cow::Borrowed(api_key().unwrap().as_str())
            ))
        );
        assert_eq!(
            pairs.next(),
            Some((Cow::Borrowed("all"), Cow::Borrowed("1")))
        );
        assert_eq!(pairs.next(), None)
    }

    #[test]
    fn request() {
        let res = get_url("sports");
        assert!(res.is_ok());
        let url = res.unwrap();
        let response = make_request(&url);
        assert!(response.is_ok());
        assert!(response.unwrap().is_array());
    }

    #[test]
    fn request_nonexistent_url() {
        let res = get_url("nonexistent");
        assert!(res.is_ok());
        let url = res.unwrap();
        let res = make_request(&url);
        assert!(res.is_err());
        // try to extract error?
        let e = res.unwrap_err();
        let desc = e.description();
        assert_eq!(
            desc,
            format!("Request to {} returned a response code 404", &url.as_str())
        );
    }

    #[test]
    fn request_non_json_response() {
        let res = get_url("");
        assert!(res.is_ok());
        let url = res.unwrap();
        let res = make_request(&url);
        assert!(res.is_ok());
        assert!(res.as_ref().unwrap().is_null());
    }

    #[test]
    #[cfg(feature = "modifies_env")]
    fn url_with_no_api_key_env_var() {
        let api_key = env::var(API_KEY_ENV_VAR_KEY);
        assert!(api_key.is_ok(), "API_KEY env var not set!");
        env::remove_var("API_KEY");
        let url = get_url("sports");
        assert!(url.is_err());
        let desc = url.as_ref().unwrap_err().description();
        assert_eq!(desc, "environment variable not found");
        env::set_var(API_KEY_ENV_VAR_KEY, api_key.unwrap());
    }

    #[test]
    #[cfg(feature = "modifies_env")]
    fn request_with_bad_api_key() {
        let api_key = env::var(API_KEY_ENV_VAR_KEY);
        assert!(api_key.is_ok(), "API_KEY env var not set!");
        env::set_var(API_KEY_ENV_VAR_KEY, "foobar");
        let url = get_url("sports");
        assert!(url.is_ok());
        let res = make_request(url.as_ref().unwrap());
        assert!(res.is_err());
        let desc = res.as_ref().unwrap_err().description();
        assert_eq!(
            desc,
            format!(
                "Request to {} returned a response code 401",
                url.as_ref().unwrap()
            )
        );
        env::set_var(API_KEY_ENV_VAR_KEY, api_key.unwrap());
    }

    #[test]
    fn sports() {
        let res = get_sports();
        assert!(res.is_ok());
    }

    #[test]
    #[cfg(feature = "uses_quota")]
    fn odds() {
        let res = get_odds("upcoming", Region::AU);
        assert!(res.is_ok());
    }

    #[test]
    #[cfg(feature = "uses_quota")]
    fn odds_missing_some_markets() {
        let sports = get_sports();
        assert!(sports.is_ok());
        let sports = sports.unwrap();
        // find AFL if it exists (may not exist all year)
        let afl = sports.iter().find(|sport| sport.sport == "AFL");
        assert!(afl.is_some(), "AFL not in list of sports.  Please update test to test for a sport that exists in a region with no markets");
        let afl = afl.unwrap();
        let res = get_odds(&afl.sport, Region::US);
        assert!(res.is_ok());
        let odds = res.unwrap();
        let mut found = false;
        odds.iter().for_each(|odd| {
            if let SiteMapOrVector::Vector(vec) = &odd.sites {
                assert!(vec.is_empty());
                found = true;
            }
        });
        assert!(found, "Test did not find any events which had no markets. Please update test to pick a sport and region which has an event with no markets")
    }

}
