use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_json::Value;


pub enum Region {
    UK,
    AU,
    US,
}

impl Region {
    pub fn as_str(&self) -> &str {
        match self {
            Region::UK => "uk",
            Region::AU => "au",
            Region::US => "us",
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Sport {
    pub sport: String,
    pub sport_group: String,
    pub display_name: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Site {
    pub last_update: u64,
    pub odds: HashMap<String, Vec<String>>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
pub enum SiteMapOrVector {
    Map(HashMap<String, Site>),
    Vector(Vec<String>),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Event {
    pub sport: String,
    pub sport_display: String,
    pub participants: [String; 2],
    pub home_team: String,
    pub commence: String,
    pub status: String,
    pub sites: SiteMapOrVector,
}