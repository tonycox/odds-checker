use std::error::Error;
use std::fmt;

pub type BoxResult<T> = Result<T, Box<Error>>;

#[derive(Debug)]
pub struct OddsCheckerError {
    details: String,
}

impl OddsCheckerError {
    pub fn new(msg: &str) -> OddsCheckerError {
        OddsCheckerError {
            details: msg.to_string(),
        }
    }

    pub fn into_box(self) -> Box<OddsCheckerError> {
        Box::new(self)
    }

    pub fn into_box_result<T>(self) -> BoxResult<T> {
        Err(self.into_box())
    }
}

impl fmt::Display for OddsCheckerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for OddsCheckerError {
    fn description(&self) -> &str {
        &self.details
    }
}

#[cfg(test)]
mod tests {}
