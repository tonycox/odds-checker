use error::{BoxResult, OddsCheckerError};

use mongodb::{Client, ThreadedClient, ClientOptions, doc, Error};
use mongodb::db::{ThreadedDatabase, Database};
use mongodb::common::{ReadPreference, ReadMode};
use mongodb::coll::results::{InsertOneResult, DeleteResult};

use std::env;
use std::thread::Thread;

const CONNECTION_STRING_ENV_VAR: &str = "ATLAS_CONNECTION_STRING";
const DB_PASSWORD_ENV_VAR: &str = "ATLAS_DB_PASSWORD";
const USER: &str = "odds-checker-webapp";
const ADMIN_DB_NAME: &str = "admin";


fn connection_string() -> BoxResult<String> {
    let res = &env::var(CONNECTION_STRING_ENV_VAR)?;
    Ok(res.to_owned())
}

fn db_password() -> BoxResult<String> {
    let res = &env::var(DB_PASSWORD_ENV_VAR)?;
    Ok(res.to_owned())
}

fn connection_is_successful<T: ThreadedDatabase>(db: &T) -> bool {
    db.list_collections(None).is_ok()
}

pub fn connect_local(db_name: &str) -> BoxResult<Database> {
    let client = Client::connect("localhost", 27017)
        .expect("Failed to initialize client.");
    let db = client.db(db_name);
    if connection_is_successful(&db) {
        Ok(db)
    } else {
        OddsCheckerError::new(&format!("Unable to connect to database {}", db_name)).into_box_result()
    }
}

pub fn connect_atlas(db_name: &str) -> BoxResult<Database> {
    let options = ClientOptions::with_unauthenticated_ssl(None, false);
    let client: Client = Client::with_uri_and_options(&connection_string()?, options)
        .expect("Failed to initialize client.");
    let db: Database = client.db(ADMIN_DB_NAME);
    let _auth = db.auth(USER, &db_password()?)?;
    let db: Database = client.db(db_name);
    if connection_is_successful(&db) {
        Ok(db)
    } else {
        OddsCheckerError::new(&format!("Unable to connect to database {}", db_name)).into_box_result()
    }
}

pub fn add_one_to_collection<T: ThreadedDatabase>(db: &T, coll_name: &str, item: bson::Document) -> Result<InsertOneResult, Error> {
    let coll = db.collection(coll_name);
    let res = coll.insert_one(item, None)?;
    Ok(res)
}

pub fn remove_one_from_collection<T: ThreadedDatabase>(db: &T, coll_name: &str, filter: bson::Document) -> Result<DeleteResult, Error> {
    let coll = db.collection(coll_name);
    let res = coll.delete_one(filter, None)?;
    Ok(res)
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_DB_NAME: &str = "testdb";
    const TEST_COLL_NAME: &str = "test_collection";

    fn set_up_test_collection<T: ThreadedDatabase>(db: &T) {
        db.drop_collection(TEST_COLL_NAME);
    }

    

    #[test]
    fn connect_and_do_nothing() {
        let res = connect_local(TEST_DB_NAME);

        assert!(res.is_ok());
    }

    #[test]
    fn add_and_remove_one() {
        let db = connect_local(TEST_DB_NAME).unwrap();
        let document: bson::Document = doc!{ "title": "Back to the Future" };

        let ins = add_one_to_collection(&db, TEST_COLL_NAME, document.clone());
        let rem = remove_one_from_collection(&db, TEST_COLL_NAME, document);

        assert!(ins.is_ok());
        assert!(rem.is_ok());
    }

    #[test]
    #[ignore]
    fn connect_atlas_and_do_nothing() {
        let res = connect_atlas(TEST_DB_NAME);

        assert!(res.is_ok());
    }

    #[test]
    #[ignore]
    fn add_and_remove_one_atlas() {
        let db = connect_atlas("testdb").unwrap();
        let document: bson::Document = doc!{ "title": "Back to the Future" };

        let ins = add_one_to_collection(&db, TEST_COLL_NAME, document.clone());
        let rem = remove_one_from_collection(&db, TEST_COLL_NAME, document);

        assert!(ins.is_ok());
        assert!(rem.is_ok());
    }

}