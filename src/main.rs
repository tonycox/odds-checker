extern crate odds_checker;
use odds_checker::odds_api::{get_odds, get_sports};
use odds_checker::models::{Region};

pub fn title(input: &str) -> String {
    let asterisks = std::iter::repeat("*").take(input.len()).collect::<String>();
    format!("\n{}\n{}\n{}", &asterisks, input.to_uppercase(), &asterisks)
}

pub fn main() {
    let sports = get_sports().unwrap();
    let events = get_odds("AFL", Region::AU).unwrap();
    println!("{}", title("sports"));
    for sport in sports {
        println!("{:?}", sport)
    }
    println!("{}", title("events"));
    for event in events {
        println!("{:?}", event)
    }
}
